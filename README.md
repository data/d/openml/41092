# OpenML dataset: Cmu_mocap_35_walk_jog

https://www.openml.org/d/41092

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Walk and jog data from CMU data base subject 35. As used in Tayor, Roweis and Hinton at NIPS 2007, but without their pre-processing (i.e. as used by Lawrence at AISTATS 2007). It consists of Subject: 35.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41092) of an [OpenML dataset](https://www.openml.org/d/41092). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41092/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41092/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41092/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

